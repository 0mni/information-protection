/*
Author : Ter-Kazaryan Artur IVBO-1-13
Encryptor/Decryptor v4.01

Что нового в v4.01:
-Добавлена поддержка верхнего регистра, теперь текст после шифровки/дешифровки не теряет регистр
-Удален массив алфавита, теперь алфавит строиться на основе монограмм
-Улучшена функция шифровки (прирост производительности ~12%)
-Улучшена функция анализа  (прирост производительности ~17%)
*/

import Cocoa

typealias Monograms = Dictionary<Character,Float>
typealias Words = Dictionary<String,Float>

let MONOGRAMS : Monograms = ["w": 0.0192, "n": 0.0709, "u": 0.0271, "v": 0.0099, "x": 0.0019,
                             "q": 0.0011, "b": 0.0154, "r": 0.0612, "c": 0.0306, "e": 0.1251,
                             "y": 0.0173, "h": 0.0549, "j": 0.0016, "p": 0.0200, "f": 0.0230,
                             "o": 0.0760, "k": 0.0067, "d": 0.0399, "t": 0.0925, "z": 0.0009,
                             "a": 0.0804, "i": 0.0726, "m": 0.0253, "s": 0.0654, "g": 0.0196,
                             "l": 0.0414]

let WORDS : Array<String> = ["the","and","her","his","was","for","you"]

func Encrypt(_ text: String, keyWord: String, MONOGRAMS: Monograms = MONOGRAMS) -> String{
    
    let ABC = MONOGRAMS.keys.sorted()
    var newABC = ABC
    var encryptDictionary : Dictionary<Character,Character> = [:]
    var encryptedText : String = ""
    
    for char in keyWord.lowercased().characters.reversed(){
        if MONOGRAMS[char] == nil {
            continue
        }
        newABC.remove(at: newABC.index(of: char)!)
        newABC.insert(char, at: 0)
    }
    
    for (index,char) in ABC.enumerated(){
        let uppercasedCharABC = Character(String(char).uppercased())
        let uppercasedCharNewABC = Character(String(newABC[index]).uppercased())
        encryptDictionary[char] = newABC[index]
        encryptDictionary[uppercasedCharABC] = uppercasedCharNewABC
    }
    
    for char in text.characters{
        encryptedText += encryptDictionary[char] == nil ? String(char) : String(encryptDictionary[char]!)
    }
    
    return encryptedText
}

func Analyze(_ text: String, MONOGRAMS : Monograms = MONOGRAMS, WORDS : Array<String> = WORDS) -> (Monograms,Words){
    var wordMaxLength: Int = 0
    var wordMinLength: Int = 0
    var lettersCount : Int = 0
    var charStream : Array<Character> = []
    var validWordLength : Bool = true
    var analyzedMonogramsDictionary : Monograms = [:]
    var analyzedWordsDictionary: Words = [:]
    
    for word in WORDS{
        wordMinLength = wordMinLength == 0 ? word.characters.count : wordMinLength
        
        wordMaxLength = word.characters.count > wordMaxLength ? word.characters.count : wordMaxLength
        wordMinLength = word.characters.count < wordMinLength ? word.characters.count : wordMinLength
        
    }
    
    for char in text.lowercased().characters{
        if MONOGRAMS[char] == nil{
            if validWordLength && charStream.count >= wordMinLength{
                let tempWord = String(charStream)
                if analyzedWordsDictionary[tempWord] == nil{
                    analyzedWordsDictionary[tempWord] = 1
                }else{
                    analyzedWordsDictionary[tempWord]! += 1
                }
            }
            charStream.removeAll()
            validWordLength = true
            continue
        }
        
        charStream.append(char)
        lettersCount += 1
        
        if analyzedMonogramsDictionary[char] == nil{
            analyzedMonogramsDictionary[char] = 1
        }else{
            analyzedMonogramsDictionary[char]! += 1
        }
        
        if !validWordLength{
            continue
        }
        
        if charStream.count > wordMaxLength{
            validWordLength = false
        }
    }
    
    for char in MONOGRAMS.keys{
        if analyzedMonogramsDictionary[char] == nil{
            analyzedMonogramsDictionary[char] = 0.0
        }else{
            analyzedMonogramsDictionary[char]! /= Float(lettersCount)
        }
    }
    
    return (analyzedMonogramsDictionary,analyzedWordsDictionary)
}

func Decrypt(_ text: String, accurary: Float = 2.0, MONOGRAMS: Monograms = MONOGRAMS, WORDS: Array<String> = WORDS) -> String{
    var (cryptedTextMonograms,cryptedTextWords) = Analyze(text)
    var cryptedTextWordsSorted : Array<(key:String,value:Float)> = []
    var decryptDictionary: Dictionary<Character,Character> = [:]
    var finalDecryptDictionary: Dictionary<Character,Character> = [:] // with uppercased characters
    var decryptedText: String = ""
    let ACCURARY = accurary / 100
    
    for word in cryptedTextWords{
        cryptedTextWordsSorted.append((word.key,word.value))
    }
    cryptedTextWordsSorted.sort(by: {$0.1>$1.1})
    
    for word in WORDS{
        for cryptedWord in cryptedTextWordsSorted{
            if word.characters.count != cryptedWord.key.characters.count{
                continue
            }
            var tempDictionary : Dictionary<Character,Character> = [:]
            
            for (index,char) in word.lowercased().characters.enumerated(){
                let temp = cryptedWord.key[cryptedWord.key.index(cryptedWord.key.startIndex, offsetBy: index)]
                if decryptDictionary[temp] != nil && decryptDictionary[temp] != char{
                    tempDictionary.removeAll()
                    break
                }
                if cryptedTextMonograms[temp]!-ACCURARY...ACCURARY+cryptedTextMonograms[temp]! ~= MONOGRAMS[char]!{
                    tempDictionary[temp] = char
                    if index == word.characters.count-1{
                        for decryptedChar in tempDictionary{
                            if decryptDictionary[decryptedChar.key] == nil{
                                decryptDictionary[decryptedChar.key] = decryptedChar.value
                            }
                        }
                        tempDictionary.removeAll()
                    }
                }else{
                    tempDictionary.removeAll()
                    break
                }
            }
        }
    }
    var newMONOGRAMS = MONOGRAMS
    for decryptedChar in decryptDictionary{
        newMONOGRAMS.removeValue(forKey: decryptedChar.value)
        cryptedTextMonograms.removeValue(forKey: decryptedChar.key)
    }
    var sortedMonograms: Array<(Character,Float)> = []
    for char in newMONOGRAMS{
        sortedMonograms.append((char.key,char.value))
    }
    
    var sortedCryptTextMonograms: Array<(Character,Float)> = []
    for char in cryptedTextMonograms{
        sortedCryptTextMonograms.append((char.key,char.value))
    }
    sortedMonograms.sort(by: {$0.1>$1.1})
    sortedCryptTextMonograms.sort(by: {$0.1>$1.1})
    
    for (index,char) in sortedCryptTextMonograms.enumerated(){
        decryptDictionary[char.0] = sortedMonograms[index].0
    }
    for char in decryptDictionary{
        let uppercasedCryptedChar = Character(String(char.0).uppercased())
        let uppercasedDecryptedChar = Character(String(char.1).uppercased())
        finalDecryptDictionary[char.key] = char.value
        finalDecryptDictionary[uppercasedCryptedChar] = uppercasedDecryptedChar
    }
    
    for char in text.characters{
        decryptedText += finalDecryptDictionary[char] == nil ? String(char) : String(finalDecryptDictionary[char]!)
    }
    return decryptedText
}